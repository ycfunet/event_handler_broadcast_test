from events import Events

import time
import random

class Seller:

    Exit = True
    Running = False

    EVENTS = None

    def __init__(self, events):
        self.EVENTS = events

    def run(self):
        self.Exit = False
        self.Running = True

        while self.Exit is False:
            vegetables_price = random.randint(10, 100)
            self.EVENTS.on_price(vegetables_price)

            time.sleep(0.1)

        self.Running = False
