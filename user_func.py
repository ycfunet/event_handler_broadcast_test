import time

class User:
    Exit = True
    Running = False

    vegetables_price = -1

    def set_price(self, price):
        self.vegetables_price = price

    def __init__(self):
        self.Exit = False
        self.Running = True

    def run(self):
        self.Exit = False
        self.Running = True

        while self.Exit is False:
            print("User Vegetables Price = " + str(self.vegetables_price))

            time.sleep(0.2)

        self.Running = False
