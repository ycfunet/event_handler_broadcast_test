from seller_func import Seller
from user_func import User
from buyer_func import Buyer

from events import Events

import threading

class Main:

    events = None

    buyer = None
    user = None
    seller = None

    def __init__(self):
        self.events = Events(('on_price', 'on_trigger'))
        self.user = User()
        self.buyer = Buyer()
        self.seller = Seller(self.events)
        self.events.on_price += self.user.set_price
        self.events.on_price += self.buyer.set_price

    def run(self):
        user_thread = threading.Thread(target=self.user.run)
        buyer_thread = threading.Thread(target=self.buyer.run)
        seller_thread = threading.Thread(target=self.seller.run)
        seller_thread.start()
        buyer_thread.start()
        user_thread.start()

if __name__ == '__main__':
    main = Main()
    main.run()
